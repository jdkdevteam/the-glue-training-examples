Exercise: 

Create a REST service using expressjs. This service needs expose endpoints to manage the following entities.

Entities:

	Users:
		- contains id, firstname, lastName, email, password
		- should be able to create, get list, get by Id
	
	Tokens:
		- contains an access token and user id
		- should create via endoint (with user credentials)

requirements:
	- use classes to handle data
	- save data in files in a performant way
	- Log all incoming request URL's and outgoing response headers
	- create a custom error handler with a 500 page. Use a view engine to display the page.
	- secure all endpoints against XSS attacks
	- add security to all endpoints, except the auth token one. Use a token based solution for this.
	
extra:
	- Add an endpoint to retrieve beers (all and byId)
	- Add an endpoint add beers to a user
	- Add an endpoint to retrieve all beers of a user.
	- Wrap your callbacks for fetching data with promises
	- instead of using files, use in memory storage (redis, ...) https://github.com/NodeRedis/node_redis
