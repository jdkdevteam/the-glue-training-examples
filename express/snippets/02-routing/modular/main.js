// main.js
const express = require('express');
const userModule = require('./users');

const app = express();
const port = 9002;

app.use('/users', userModule);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
