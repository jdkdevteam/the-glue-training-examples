// users.js
const express = require('express');

const router = express.Router();

router.get('/', (req, res) => {
	res.json([{name: 'Mike'}, {name: 'Cornel'}])
});

router.get('/:userId', (req, res) => {
	res.send({id: req.params.userId, name: 'Mike'})
});

router.post('/', (req, res) => {
	res.status(201).send('You created a user!')
});

router.delete('/', (req, res) => {
	res.send('You deleted a user!')
});

module.exports = router;
