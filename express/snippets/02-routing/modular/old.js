// express
const express = require('express');

const app = express();
const port = 9002;

app.get('users/', () => {});
app.get('users/:userId\'', () => {});
app.post('users/', () => {});
app.delete('users/', () => {});

app.get('books/', () => {});
app.get('books/:bookId', () => {});
app.post('books/', () => {});
app.delete('books/', () => {});

// ...

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
