// queries
const express = require('express');

const app = express();
const port = 9002;

app.get('/*', (req, res) => {
	console.log(req.query);
	res.send(req.query);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

// /random/path?foo=bar ==> { foo: 'bar' }
