// parameters
const express = require('express');

const app = express();
const port = 9002;

app.get('/users/:userId/languages/:languageId', (req, res) => {
	console.log(req.params);
	res.send(req.params);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

// /users/foo/languages/bar' ==> { userId: 'foo', languageId: 'bar' }
