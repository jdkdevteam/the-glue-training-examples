// static-files
const path = require('path');
const express = require('express');

const app = express();
const port = 9002;

app.use('/static', express.static(path.join(__dirname, 'assets')));

app.get('/', (req, res) => {
	res.send('Received a GET request!');
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

// http://localhost:9002/static/images/image.jpg
