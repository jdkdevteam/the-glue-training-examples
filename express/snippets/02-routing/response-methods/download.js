// response-methods
const path = require('path');
const express = require('express');

const app = express();
const port = 9002;

app.get('/', (req, res) => {
	const filePath = path.join(__dirname, 'assets', 'images', 'image.jpg');

	res.download(filePath);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

