// response-methods
const express = require('express');

const app = express();
const port = 9002;

app.get('/', (req, res) => {
	res.set('Authorization', `Bearer this-is-some-super-secure-token`);
	res.end();
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

// Authorization: Bearer this-is-some-super-secure-token

