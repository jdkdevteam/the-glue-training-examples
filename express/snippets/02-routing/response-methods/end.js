// response-methods
const express = require('express');

const app = express();
const port = 9002;

app.get('/', (req, res) => {
	res
		.status(418) // I'm a teapot
		.end();
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
