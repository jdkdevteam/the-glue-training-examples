// response-methods
const express = require('express');

const app = express();
const port = 9002;

app.get('/', (req, res) => {
	const redirectUrl = 'http://www.jidoka.be/';

	res.redirect(redirectUrl);
});

app.get('/404', (req, res) => {
	res
		.status(404)
		.send('404 page!');
});

app.get('/*', (req, res) => {
	res.redirect('/404');
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
