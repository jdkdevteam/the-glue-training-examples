// response-methods
const express = require('express');

const app = express();
const port = 9002;

app.get('/', (req, res) => {
	const jsonObject = {
		foo: 'bar'
	};

	res.json(jsonObject);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
