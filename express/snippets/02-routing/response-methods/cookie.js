// response-methods
const express = require('express');

const app = express();
const port = 9002;

app.get('/', (req, res) => {
	const token = {access_token: 'super-secure-token-here'};

	res.cookie('oauth-token', token, {domain: '.jidoka.com', path: '/', secure: true});
	res.end();
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

// Set-Cookie: oauth-token=j%3A%7B%22access_token%22%3A%22super-secure-token-here%22%7D; Domain=.jidoka.com; Path=/; Secure

