// response-methods
const express = require('express');

const app = express();
const port = 9002;

app.get('/', (req, res) => {
	res
		.send('<html><body><h1>HTML Example</h1></body></html>')
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
