// basics-all
const express = require('express');

const app = express();
const port = 9002;

app.all('/', (req, res) => {
	res.send(`Received a ${req.method.toUpperCase()} request!`);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
