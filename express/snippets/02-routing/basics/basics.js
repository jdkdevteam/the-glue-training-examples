// basics
const express = require('express');

const app = express();
const port = 9002;

app.get('/', (req, res) => {
	res.send('Received a GET request!');
});

app.post('/', (req, res) => {
	res.send('Received a POST request');
});

app.get('/users', (req, res) => {
	res.send('Received a GET request for users');
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
