// basics-chaining
const express = require('express');

const app = express();
const port = 9002;

app.route('/users')
	.get((req, res) => {
		res.json([{user: 'Cornel'}, {user: 'Mike'}]);
	})
	.post((req, res) => {
		res
			.status(201)
			.end();
	});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
