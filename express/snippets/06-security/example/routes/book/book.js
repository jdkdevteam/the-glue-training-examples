// users.js
const express = require('express');

const router = express.Router();

router.get('/', (req, res) => {
	res.json([{title: 'ExpressJS in a nutshell'}, {title: 'Auth for dummies'}])
});

router.get('/:bookId', (req, res) => {
	res.send({id: req.query.bookId, title: 'ExpressJS in a nutshell'})
});

router.post('/', (req, res) => {
	res.status(201).send('You created a book!')
});

module.exports = router;
