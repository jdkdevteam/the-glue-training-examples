// users.js
const express = require('express');

const router = express.Router();

router.get('/token', (req, res) => {
	const userId = req.query.userId;
	const password = req.query.password;

	// create token based on credentials here ...

	res.json({
		token: 'super-secure-token-here'
	})
});

module.exports = router;
