// express
const authMiddleware = (req, res, next) => {
	if (req.get('Authorization')) {
		// check if token valid

		next();
	} else {
		res.status(401).end();
	}
};

module.exports = authMiddleware;
