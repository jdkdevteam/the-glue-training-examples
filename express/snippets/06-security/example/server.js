// express
const express = require('express');
const helmet = require('helmet');

const authModule = require('./routes/auth/auth');
const userModule = require('./routes/user/user');
const bookModule = require('./routes/book/book');

const app = express();
const port = 9002;

app.use(helmet()); // defaults

app.use(helmet({
	// ...config
}));

app.use(helmet.xssFilter());

app.use('/auth', authModule);
app.use('/users', userModule);
app.use('/books', bookModule);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
