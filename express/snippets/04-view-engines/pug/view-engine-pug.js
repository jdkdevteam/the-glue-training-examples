// express
const express = require('express');
const path = require('path');

const app = express();
const port = 9002;

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, '/views'));

app.get('/', (req, res) => {
	res.render('pages/home');
});


app.get('/about', (req, res) => {
	const data = {
		users: [{
			name: 'Cornel',
			age: 25
		}, {
			name: 'Mike'
		}]
	};

	res.render('pages/about', data);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
