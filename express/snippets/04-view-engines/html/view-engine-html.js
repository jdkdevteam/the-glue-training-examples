// express
const express = require('express');
const path = require('path');

const app = express();
const port = 9002;

const templatePath = path.join(__dirname, 'template.html');

app.get('/', (req, res) => {
	res.sendFile(templatePath);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
