// basics-middleware-logging
const express = require('express');
const logginMiddleware = require('./logging-middleware');

const app = express();
const port = 9002;

app.use(express.json());
app.use(logginMiddleware);

app.route('/users')
	.get((req, res) => {
		res.send([{name: 'Mike'}, {name: 'Cornel'}]);
	})
	.post((req, res) => {
		res.send(req.body);
	});


app.listen(port, () => console.log(`Example app listening on port ${port}!`));
