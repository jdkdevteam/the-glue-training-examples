// basic
const express = require('express');

const app = express();
const port = 9002;

app.route('/users')
	.get((req, res) => {
		res.send([{name: 'Mike'}, {name: 'Cornel'}]);
	})
	.post((req, res) => {
		let body = '';

		req.on('data', (chunk) => {
			body += chunk;
		});

		req.on('end', () => {
			console.log(body);
			res.send(body);
		});
	});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

// {"name": "Cornel"}
// text/html
