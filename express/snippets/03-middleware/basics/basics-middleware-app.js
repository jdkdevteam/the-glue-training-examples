// basics-middleware-app
const express = require('express');

const app = express();
const port = 9002;

const parseFn = ((req, res, next) => {
	req.rawBody = '';

	req.on('data', (chunk) => {
		req.rawBody += chunk;
	});

	req.on('end', () => {
		next()
	});
});

app.use(parseFn);

app.route('/users')
	.get((req, res) => {
		res.send([{name: 'Mike'}, {name: 'Cornel'}]);
	})
	.post((req, res) => {
		res.send(req.rawBody);
	});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

// {"name": "Cornel"}
// text/html
