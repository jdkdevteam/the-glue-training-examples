// basics-middleware-json
const express = require('express');

const app = express();
const port = 9002;

app.use(express.json());

app.route('/users')
	.get((req, res) => {
		res.send([{name: 'Mike'}, {name: 'Cornel'}]);
	})
	.post((req, res) => {
		res.send(req.body);
	});


app.listen(port, () => console.log(`Example app listening on port ${port}!`));
