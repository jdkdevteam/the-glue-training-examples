// express
const fs = require('fs');
const express = require('express');

const app = express();
const port = 9002;

app.get('/', (req, res, next) => {
	fs.readFile('/unkown-path/test.txt', (err, data) => {
		if (err) {
			next(err); // pass errors to express
		} else {
			res.send(data);
		}
	})
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
