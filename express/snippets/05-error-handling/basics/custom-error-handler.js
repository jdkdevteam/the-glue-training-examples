// express
const express = require('express');

const app = express();
const port = 9002;

app.get('/', (req, res) => {
	throw new Error('Major issue here!'); // handled by express

	res.send('Hello World!');
});

app.use((err, req, res, next) => {
	console.error(err.stack);
	res.status(500).send('Shit hit the fan!');
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
