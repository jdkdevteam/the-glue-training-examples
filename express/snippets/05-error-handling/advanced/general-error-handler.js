// general-error-handler

const generalErrorHandler = (err, req, res, next) => {
	console.error(err.stack);
	res.redirect('/500');
};

module.exports = generalErrorHandler;
