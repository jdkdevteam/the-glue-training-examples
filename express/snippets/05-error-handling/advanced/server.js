// express
const express = require('express');
const authErrorHandler = require('./auth-error-handler');
const generalErrorHandler = require('./general-error-handler');

const app = express();
const port = 9002;

app.get('/', (req, res) => {
	throw new Error('Major issue here!'); // handled by express

	res.send('Hello World!');
});

app.get('/500', (req, res) => {
	res.status(500).send('Error page!');
});


app.use(authErrorHandler);
app.use(generalErrorHandler);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
