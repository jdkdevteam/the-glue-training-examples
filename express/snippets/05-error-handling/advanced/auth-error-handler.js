// auth-error-handler

const authErrorHandler = (err, req, res, next) => {
	if (req.header('Authorization')) {
		res.status(500).send('Your authenticated request failed!');
	} else {
		next(err)
	}
};

module.exports = authErrorHandler;
