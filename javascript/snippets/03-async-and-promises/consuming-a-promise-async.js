const asyncOperation = () => {
	return new Promise((resolve, reject) => {
		setTimeout(() => resolve('foo'), 5000);
	});
};

asyncOperation()
	.then(value => {
		console.log(value);
	});

console.log('Logging some info here ...');

// Logging some info here ...
// foo --> after 5 seconds
