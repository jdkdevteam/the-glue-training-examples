const fetchCompany = () => Promise.resolve({company: 'Jidoka'});

const fetchEmployee = (company) => Promise.resolve({name: 'Cornel', company});

const fetchHobbies = (employee) => Promise.resolve({employee, hobbies: ['Music']});

fetchCompany()
	.then(company => fetchEmployee(company))
	.then(employee => fetchHobbies(employee))
	.then(employee => {
		console.log(employee);
	});

// { employee: { name: 'Cornel', company: { company: 'Jidoka' } }, hobbies: [ 'Music' ] }

fetchCompany()
	.then(fetchEmployee)
	.then(fetchHobbies)
	.then(employee => {
		console.log(employee);
	});
