const fetchCompany = async () => ({company: 'Jidoka'});

const fetchEmployee = async (company) => {
	throw new Error('foo')
};

const fetchHobbies = async (employee) => ({employee, hobbies: ['Music']});

async function init() {
	try {
		const company = await fetchCompany();
		let employee = await fetchEmployee(company);
		employee = await fetchHobbies(employee);

		console.log(employee);
	} catch (error) {
		console.log(error)
	}

}

init();

