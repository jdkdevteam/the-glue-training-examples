const asyncOperationA = () => {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			resolve('foo');
		}, 10000000);
	});
};

const asyncOperationB = () => {
	return new Promise((resolve, reject) => {
		resolve('bar');
	});
};

Promise.race([asyncOperationA(), asyncOperationB()])
	.then((value) => {
		console.log(value); // bar
	});
