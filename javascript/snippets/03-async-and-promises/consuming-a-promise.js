const asyncOperation = () => {
	return new Promise((resolve, reject) => {
		// some more code here ...
	});
};

asyncOperation()
	.then(value => {
		console.log(value);
	})
	.catch(error => {
		console.error(error);
	})
	.finally(() => {
		console.log('Finally its done!');
	});
