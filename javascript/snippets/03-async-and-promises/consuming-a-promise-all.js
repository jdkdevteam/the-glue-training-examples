const asyncOperationA = () => {
	return new Promise((resolve, reject) => {
		resolve('foo');
	});
};

const asyncOperationB = () => {
	return new Promise((resolve, reject) => {
		resolve('bar');
	});
};

Promise.all([asyncOperationA(), asyncOperationB()])
	.then((values) => {
		console.log(values[0]); // foo
		console.log(values[1]); // bar
	});
