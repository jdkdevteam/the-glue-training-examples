const asyncOperation = () => {
	return new Promise((resolve, reject) => {
		// some more code here ...

		// resolve('foo'); // resolves the promise
		reject('bar'); // rejects the promise
	});
};

asyncOperation()
	.then(value => {
		console.log(value); // foo
	})
	.catch(error => {
		console.error(error); // bar
	});
