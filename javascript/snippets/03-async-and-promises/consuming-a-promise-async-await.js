const fetchCompany = async () => ({company: 'Jidoka'});

const fetchEmployee = async (company) => ({name: 'Cornel', company});

const fetchHobbies = async (employee) => ({employee, hobbies: ['Music']});

async function init() {
	const company = await fetchCompany();
	let employee = await fetchEmployee(company);
	employee = await fetchHobbies(employee);

	console.log(employee);
}

init();

// { employee: { name: 'Cornel', company: { company: 'Jidoka' } }, hobbies: [ 'Music' ] }
