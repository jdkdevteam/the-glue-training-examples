const asyncOperation = () => {
	return Promise.resolve('foo'); // resolves the promise

	return Promise.reject('bar'); // rejects the promise
};

asyncOperation()
	.then(value => {
		console.log(value); // foo
	})
	.catch(error => {
		console.error(error); // bar
	});
