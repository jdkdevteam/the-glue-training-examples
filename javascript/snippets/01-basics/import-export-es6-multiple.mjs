import {foo, lorem} from './import-export-es6-fn-multiple';

console.log(foo()); // bar
console.log(lorem()); // ipsum
