const javascript = {
	name: 'Javascript',
	awesome: true
};

const javascriptCopy = {
	name: 'Javascript',
	awesome: true
};

console.log(javascript === javascriptCopy);
console.log(JSON.stringify(javascript) === JSON.stringify(javascriptCopy));

// false
// true
