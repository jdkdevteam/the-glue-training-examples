// scopes: global
let foo = 'bar';
if (foo === 'bar') {
	foo = 'lorem';
}

console.log(foo); // lorem

// scopes: local
if (foo === 'bar') {
	var foo = 'bar';
	let duke = 'nukem';
}

console.log(foo); // bar
console.log(duke); // unresolved variable duke

// scopes: nested

// global scope
function foo() {
	// scope foo
	function bar() {
		// scope bar
	}
}
