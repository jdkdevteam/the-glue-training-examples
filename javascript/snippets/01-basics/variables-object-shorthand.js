const awesomeLevel = '9000+';

const javascript = {
	name: 'Javascript',
	awesome: true,
	awesomeLevel
};

console.log(javascript.name); // Javascript
console.log(javascript.awesome); // true
console.log(javascript['awesomeLevel']); // 9000+
