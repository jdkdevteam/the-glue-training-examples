const foo = () => 'bar';

const lorem = () => 'ipsum';

module.exports = {
	foo,
	lorem
};
