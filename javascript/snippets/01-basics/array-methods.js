const languages = ['Javascript', 'Java', 'Python'];

const objects = languages
	.map(language => ({
		name: language
	}))
	.filter(language => language.name !== 'Python');

const js = objects.find(language => language.name === 'Javascript');
const hasJava = objects.some(language => language.name === 'Java');

console.log(js.name); // Javascript
console.log(hasJava); // true
