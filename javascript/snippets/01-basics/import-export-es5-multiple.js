const {foo, lorem} = require('./import-export-es5-fn-multiple');

console.log(foo()); // bar
console.log(lorem()); // ipsum
