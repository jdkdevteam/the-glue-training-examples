// dynamically typed
let variable = 1;
variable = {foo: 'bar'};
variable = "Toyota";
variable = ["Saab", "Volvo", "BMW"];

// variable types
const aNumber = 12.55;
const aString = 'foo bar!';
const anotherString = "foo bar!";
const aBoolean = true;
const anObject = {
	foo: 'bar!'
};
const aSymbol = Symbol('foo');
const anArray = ['foo', 'bar'];
