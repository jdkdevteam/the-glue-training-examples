try {
	console.log('foo!');

	throw new Error('This is a thrown error!');

	console.log('bar');
} catch (error) {
	console.log(error.message);
}

// foo!
// This is a thrown error!

try {
	console.log('foo!');
	throw new Error('error!');
	console.log('bar');
} catch (error) {
	console.log(error.message);
} finally {
	console.log('Firing my lazor here');
}

// foo!
// This is a thrown error!
// Firing my lazor here
