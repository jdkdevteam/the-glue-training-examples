const {calculate} = require('./index');
const RandomService = require('./service/random.service');

describe('test: index.js', () => {
	test('should calculate correctly', () => {
		const spyFn = jest.spyOn(RandomService.prototype, 'random');

		const result = calculate();

		expect(result).toBe(-24);
		expect(spyFn).toHaveBeenCalledTimes(2);
	});
});
