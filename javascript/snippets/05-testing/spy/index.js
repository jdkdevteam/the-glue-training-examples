// index.js
const RandomService = require('./service/random.service');

const randomService = new RandomService();

const calculate = () => {
	return randomService.random() + randomService.random();
};

module.exports = {
	calculate
};
