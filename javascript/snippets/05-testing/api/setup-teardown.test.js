const {sum} = require('./index');

describe('test: index.js', () => {

	beforeEach(() => {
		// initialize database here
	});

	afterEach(() => {
		// clear database here
	});

	describe('function: sum', () => {
		beforeEach(() => {
			// set base variables here
		});

		test('should sum 2 positive numbers', () => {
			const result = sum(5, 7);

			expect(result).toBe(12);
		});
	});
});
