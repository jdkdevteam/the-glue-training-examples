const {sum} = require('./index');

describe('test: assertions.js', () => {
	test('should sum 2 positive numbers', () => {
		const result = sum(5, 7);

		expect(result).toBe(12);
		expect(result).toBeTruthy();
		expect(result).not.toEqual(33);
	});
});
