describe('test: mock-function.js', () => {
	test('should call mock function', () => {
		const mockFunction = jest.fn((param) => 25 * param);

		const result = mockFunction(2);

		expect(result).toBe(50);
		expect(mockFunction).toHaveBeenCalled();
		expect(mockFunction).toHaveBeenCalledWith(2);
	});
});
