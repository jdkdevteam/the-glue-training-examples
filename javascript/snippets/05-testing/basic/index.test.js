const {sum, multiply} = require('./index');

describe('test: index.js', () => {
	describe('function: sum', () => {
		test('should sum 2 positive numbers', () => {
			const result = sum(5, 7);

			expect(result).toBe(12);
		});

		test('should sum 2 negative numbers', () => {
			const result = sum(-5, -7);

			expect(result).toBe(-12);
		});
	});

	describe('function: multiply', () => {
		test('should multiply 2 positive numbers', () => {
			const result = multiply(5, 7);

			expect(result).toBe(35);
		});

		test('should sum 2 negative numbers', () => {
			const result = multiply(5, 7);

			expect(result).toBe(35);
		});
	});
});
