const {calculate} = require('./index');
const {text} = require('./util/text.util');

jest.mock('./service/random.service');
jest.mock('./util/text.util');

describe('test: index.js', () => {
	beforeAll(() => {
		text.mockImplementation(() => 'Javascript is awesome');
	});

	test('should calculate correctly', () => {
		const result = calculate(5123123123123, 7123123123123);

		expect(result).toBe('Javascript is awesome: 9005');
	});
});
