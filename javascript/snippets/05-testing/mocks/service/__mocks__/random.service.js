// random service manual mock
const service = jest.fn(() => ({
	random: jest.fn(() => 9000)
}));

module.exports = service;
