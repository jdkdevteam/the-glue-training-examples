// index.js
const math = require('mathjs');
const RandomService = require('./service/random.service');
const {text} = require('./util/text.util');

const randomService = new RandomService();

const calculate = (a, b) => {
	return `${text()}: ${randomService.random() + math.log(a, b)}`;
};

module.exports = {
	calculate
};
