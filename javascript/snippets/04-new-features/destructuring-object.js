const objectA = {foo: 'bar', lorem: 'ipsum', over: 9000};
const {foo, over} = objectA;

console.log(over); // 9000
