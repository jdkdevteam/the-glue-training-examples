function * generatorFn(start) {
	let total = start;

	do {
		total += 10;
		yield total;
	} while (total <= 100)
}

const fn = generatorFn(5);

console.log(fn.next().value); // 15
console.log(fn.next().value); // 25
console.log(fn.next().value); // 35
console.log(fn.next().value); // 45
console.log(fn.next().value); // 55
