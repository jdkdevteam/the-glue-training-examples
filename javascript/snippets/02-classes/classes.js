class Foo {
	constructor(name) {
		this.name = name;
	}

	print() {
		console.log('name: ' + this.name);
	}
}

class Bar extends Foo {
	constructor(name, age) {
		super(name);
		this.age = age;
	}

	printName() {
		super.print();
	}

	print() {
		console.log('name: ' + this.name + ', age: ' + this.age);
	}

}

const foo = new Foo('foo');
const bar = new Bar('bar', 18);

bar.printName(); // name: bar
bar.print(); // name: bar, age: 18
