class Foo {
	constructor() {
		this._name = 'bar';
	}

	get name() {
		return this._name;
	}

	set name(value) {
		this._name = value;
	}
}

const foo = new Foo();
console.log(foo.name); // bar

foo.name = 'insert fancy string here';
console.log(foo.name); // insert fancy string here
