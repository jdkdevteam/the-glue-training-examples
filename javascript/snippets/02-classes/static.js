class MathUtil {
	static ZERO = 0; // nodejs 12

	static get ZERO() {
		return 0;
	}

	static sum(a, b) {
		return a + b;
	}

	static subtract(a, b) {
		return a - b;
	}
}

console.log(MathUtil.sum(1, 2)); // 3
console.log(MathUtil.subtract(1, 2)); // -1
console.log(MathUtil.ZERO); // 0
