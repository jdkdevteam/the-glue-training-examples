"use strict";

// index.js
var sum = function sum(a, b) {
  return a + b;
};

var multiply = function multiply(a, b) {
  return a * b;
};

module.exports = {
  sum: sum,
  multiply: multiply
};