module.exports = {
    "env": {
        "browser": true,
        "node": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 2018
    },
    "rules": {
        "camelcase": "error",
        "complexity": "error",
        "eqeqeq": "error"
    }
};
