Exercise: rewrite exercise 3 using the new features seen in this chapter.

requirements:
- use async/await for http
- use destructing where possible
- use a generator function to loop the random brewery generation multiple times.
