Exercise: Create a random beer brewery generator.

requirements:
- Fetch the breweries from the public API (https://api.openbrewerydb.org/breweries)
- Print the required output in the browser console

extra:
- use the reverse geocoding API of OpenSteetMap to retrieve extra address information and print it in the browser console. Wait until all info is retrieved before you generate the random brewery.
  example: https://nominatim.openstreetmap.org/reverse?lat={latitude}&lon={longitude}&format=json

example output:

**************************************************
Your random number is: 2
Your random brewery is: 
id: 44
name: Trim Tab Brewing
brewery type: micro
street: 2721 5th Ave S
city: Birmingham
state: Alabama
postal code: 35233-3401
country: United States
phone: 2057030536
**************************************************
