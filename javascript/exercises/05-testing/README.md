Exercise: Create unit tests for the provided code.

requirements:
- use manual mocks
- use auto mocks
- fully test the Application and CalculatorService classes.

extra:
- verify the console output in Application. 
  HINT: setup file and global mock.
