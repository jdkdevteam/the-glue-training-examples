const CalculatorService = require('./service/CalculatorService');

class Application {
	init() {
		const calculatorService = new CalculatorService();
		const value = calculatorService.calculate();

		console.log(`The calculated value is ${value}`);
	}
}

module.exports = Application;
