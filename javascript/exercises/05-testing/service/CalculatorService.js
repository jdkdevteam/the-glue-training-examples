const random = require('random');
const math = require('mathjs');
const numbers = require('numbers');
const numeric = require('numeric');

class CalculatorService {

	constructor() {
		math.import(numbers, {wrap: true, silent: true});
		math.import(numeric, {wrap: true, silent: true});
	}

	calculate() {
		const randomNumber = random.int(0, 100);
		const fib = math.fibonacci(randomNumber);

		return fib;
	}
}


module.exports = CalculatorService;
