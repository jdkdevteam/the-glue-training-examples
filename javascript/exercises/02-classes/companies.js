module.exports = [{
	name: 'vondale Brewing Co',
	type: 'brewery',
	brewery_type: 'micro',
	address: {
		country: 'United States',
	}
}, {
	name: 'Band of Brothers Brewing Company',
	type: 'brewery',
	brewery_type: 'micro',
	address: {
		country: 'United States',
	}
}, {
	name: 'Trim Tab Brewing',
	type: 'brewery',
	brewery_type: 'micro',
	address: {
		country: 'United States',
	}
}, {
	name: 'Bearpaw River Brewing Co',
	type: 'brewery',
	brewery_type: 'brewpub',
	address: {
		country: 'United States',
	}
}, {
	name: 'King Street Brewing Co',
	type: 'brewery',
	brewery_type: 'micro',
	address: {
		country: 'United States',
	}
}, {
	name: '1912 Brewing',
	type: 'brewery',
	brewery_type: 'micro',
	address: {
		country: 'United States',
	}
}, {
	name: 'Bad Water Brewing',
	type: 'brewery',
	brewery_type: 'micro',
	address: {
		country: 'United States',
	}
}, {
	name: 'BJs Restaurant & Brewery - Chandler',
	type: 'brewery',
	brewery_type: 'micro',
	address: {
		country: 'United States',
	}
}, {
	name: 'BlackRock Brewers',
	type: 'brewery',
	brewery_type: 'micro',
	address: {
		country: 'United States',
	}
}, {
	name: 'Grand Canyon Brewing Company',
	type: 'brewery',
	brewery_type: 'micro',
	address: {
		country: 'United States',
	}
}, {
	name: 'Mudshark Brewing Co',
	type: 'brewery',
	brewery_type: 'micro',
	address: {
		country: 'United States',
	}
}, {
	name: 'Richter Aleworks',
	type: 'brewery',
	brewery_type: 'micro',
	address: {
		country: 'United States',
	}
}, {
	name: 'SanTan Brewing Co',
	type: 'brewery',
	brewery_type: 'micro',
	address: {
		country: 'United States',
	}
}, {
	name: 'Wren House Brewing Company',
	type: 'brewery',
	brewery_type: 'micro',
	address: {
		country: 'United States',
	}
}, {
	name: 'Brick Oven Pizza Co / Brick & Forge Brewing',
	type: 'brewery',
	brewery_type: 'micro',
	address: {
		country: 'United States',
	}
}, {
	name: 'Diamond Bear Brewing Co',
	type: 'brewery',
	brewery_type: 'contract',
	address: {
		country: 'United States',
	}
}, {
	name: 'Jidoka',
	type: 'it',
	description: 'Let\'s face the future together',
	address: {
		country: 'United States',
	}
}];
