Exercise: Create a random beer brewery generator by using object oriented programming features like classes. You can reuse previous exercise.

requirements:
- Use inheritance to create a Company class with Brewery and ITCompany subclasses.
- Create a constant type for verifying the type of company.
- One class per file
- use method overwriting to minimise code duplication
- use static constructor functions for handling the creation of the objects.

example output for brewery:

**************************************************
Your random number is: 0
Your random brewery is: 
Name: Diamond Bear Brewing Co
Country: United States
Company type: brewery
Brewery type: contract
**************************************************

example output for IT company:

**************************************************
Your random number is: 1
Your random brewery is: 
Name: Jidoka
Country: United States
Company type: it
Description: Let's face the future together
**************************************************

	
