module.exports = [{
	name: 'vondale Brewing Co',
	address: {
		country: 'United States',
	}
}, {
	name: 'Band of Brothers Brewing Company',
	address: {
		country: 'United States',
	}
}, {
	name: 'Trim Tab Brewing',
	address: {
		country: 'United States',
	}
}, {
	name: 'Yellowhammer Brewery',
}, {
	name: 'Bearpaw River Brewing Co',
	address: {
		country: 'United States',
	}
}, {
	name: 'King Street Brewing Co',
	address: {
		country: 'United States',
	}
}, {
	name: '1912 Brewing',
	address: {
		country: 'United States',
	}
}, {
	name: 'Bad Water Brewing',
	address: {
		country: 'United States',
	}
}, {
	name: 'BJs Restaurant & Brewery - Chandler',
	address: {
		country: 'United States',
	}
}, {
	name: 'BlackRock Brewers',
	address: {
		country: 'United States',
	}
}, {
	name: 'Dragoon Brewing Co',
}, {
	name: 'Grand Canyon Brewing Company',
	address: {
		country: 'United States',
	}
}, {
	name: 'Mudshark Brewing Co',
	address: {
		country: 'United States',
	}
}, {
	name: 'Richter Aleworks',
	address: {
		country: 'United States',
	}
}, {
	name: 'SanTan Brewing Co',
	address: {
		country: 'United States',
	}
}, {
	name: 'State 48 Brewery',
}, {
	name: 'Wren House Brewing Company',
	address: {
		country: 'United States',
	}
}, {
	name: 'Brick Oven Pizza Co / Brick & Forge Brewing',
	address: {
		country: 'United States',
	}
}, {
	name: 'Diamond Bear Brewing Co',
	address: {
		country: 'United States',
	}
}, {
	name: 'Lost Forty Brewing',
}];
