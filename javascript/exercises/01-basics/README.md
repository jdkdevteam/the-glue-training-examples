Exercise: Create a random beer brewery generator with correct error handling and output the brewery name and country

tips: 
- Math.random
- require(...)

example output:

**************************************************
Your random number is: 5
Your random brewery is:
vondale Brewing Co - United States
**************************************************

Or on error:

**************************************************
Your random number is: 10
Error while outputting your brewery. Retrying another random brewery.
Your random number is: 10
Error while outputting your brewery. Retrying another random brewery.
Your random number is: 4
Your random brewery is:
Name: Bearpaw River Brewing Co
Country: United States
**************************************************

	
