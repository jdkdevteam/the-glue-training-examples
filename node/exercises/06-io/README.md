Exercise: 

Create a contact form (firstName, lastName, email, content) that is hosted on a nodejs server. 
Every time a request is received, the contact form data is saved to a file. 

when accessing /data, all stored data is displayed to the user.

- requirements
	- create webserver to handle request using node http module
	- read html from a file
	- save data to a file in a non-blocking way. 1 post = 1 line
	- use the lorem-ipsum npm library to generate some content for the html page. You can use placeholders to inject the dynamic data for each request.

- extra:
	- serve the contact form again after the user submitted his data
	
https://www.npmjs.com/package/lorem-ipsum


	
