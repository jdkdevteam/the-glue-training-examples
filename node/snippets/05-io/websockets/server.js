const http = require('http');
const io = require('socket.io');

const server = http.createServer((req, res) => {
	res.writeHead(200, {'Content-Type': 'text/html'});
	res.end('Hello world!');
});
server.listen(9002);


const socket = io.listen(server);

setInterval(() => {
	console.log('Sending data to client');
	socket.emit('time', {
		data: new Date().getTime()
	});
}, 2000);

socket.on('connection', (client) => {
	client.on('data', (event) => {
		console.log('Received data from client:', event);
	});

	client.on('disconnect', () => {
		console.log('disconnect');
	});
});
