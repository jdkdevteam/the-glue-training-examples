// readLines.js
const fs = require('fs');
const path = require('path');
const readline = require('readline');

// build absolute path
const filePath = path.join(__dirname, 'data.json');

const lines = [];

const readInterface = readline.createInterface({
	input: fs.createReadStream(filePath)
});

readInterface.on('line', (line) => {
	lines.push(line);
});

readInterface.on('close', () => {
	console.log(lines);
});

readInterface.on('error', (error) => {
	console.error(error);
});

// output:
// [ '{', '  "foo": "bar"', '}' ]
