// readStream.js
const fs = require('fs');
const path = require('path');

// build absolute path
const filePath = path.join(__dirname, 'data.json');

// read file
const fileStream = fs.createReadStream(filePath);

fileStream.on('end', () => {
	console.log('example: readStream');
});

fileStream.pipe(process.stdout);

// output:
// {
// 	"foo": "bar"
// }
// example: readStream
