// readSync.js
const fs = require('fs');
const path = require('path');

// build absolute path
const filePath = path.join(__dirname, 'data.json');

// read file
const data = fs.readFileSync(filePath).toString();

console.log(data);
console.log('example: readSync');

// output:
// {
// 	"foo": "bar"
// }
//
// example: readSync
