// readAsync.js
const fs = require('fs');
const path = require('path');

// build absolute path
const filePath = path.join(__dirname, 'data.json');

// read file
fs.readFile(filePath, (err, buf) => {
	console.log(buf.toString());
});

console.log('example: readAsync');

// output:
// example: readAsync
// {
// 	"foo": "bar"
// }
