// writeAsync.js
const fs = require('fs');
const path = require('path');

// build absolute path
const filePath = path.join(__dirname, 'output.json');

// write file
fs.writeFile(filePath, "{\"Lorem\": \"Ipsum\"}", (err) => {
	if(err) {
		console.log(err);
	} else {
		console.log("Done!");
	}
});
