const http = require('http');

http.createServer((request, response) => {
	if (request.method === 'POST') {
		let body = '';

		request.on('data', function(data) {
			body += data
		});
		request.on('end', function() {
			response.writeHead(200, {'Content-Type': 'application/json'});
			response.end(`{\"data\": \"${body}\"}`)
		});
	} else {
		const html = `
            <html>
                <body>
                    <form method="post" action="http://localhost:9002">Data: 
                        <input type="text" name="data" />
                        <input type="submit" value="Send" />
                    </form>
                </body>
            </html>`;

		response.writeHead(200, {'Content-Type': 'text/html'});
		response.end(html)
	}
}).listen(9002);
