// native.js
const axios = require('axios');

const url = 'https://api.openbrewerydb.org/breweries';

(async () => {
	const res = await axios.get(url);
	console.log(res);
})();
