// native.js
const https = require("https");

const url = 'https://api.openbrewerydb.org/breweries';

https.get(url, res => {
	let body = '';

	res.on('data', data => {
		body += data;
	});
	res.on('end', () => {
		body = JSON.parse(body);

		console.log(body);
	});
});
