// native.js
const https = require("https");
const querystring = require("querystring");

const url = 'https://postman-echo.com/post';

const data = querystring.stringify({
	'foo': 'bar'
});

const options = {
	method: 'POST',
	headers: {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Content-Length': Buffer.byteLength(data)
	}
};

const request = https.request(url, options, res => {
	console.log(`status: ${res.statusCode}`);

	let body = '';

	res.on('data', data => {
		body += data;
	});

	res.on('end', () => {
		console.log(data);
	});
});

request.write(data);
request.end();
