// native.js
const http = require("http");
const url = require('url');

http.createServer((req, res) => {
	const query = url.parse(req.url, true).query;

	res.writeHead(200, {'Content-Type': 'application/json'});

	res.write(`{\"foo\": \"${query.value}\"}`);

	res.end();
}).listen(9002);
