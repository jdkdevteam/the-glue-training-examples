// native.js
const http = require("http");

const url = 'http://localhost:9002/foo?value=bar';

http.get(url, res => {
	let body = '';

	res.on('data', data => {
		body += data;
	});
	res.on('end', () => {
		body = JSON.parse(body);

		console.log(body);
	});
});

// response:
// { foo: 'bar' }
