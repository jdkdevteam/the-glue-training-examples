class Welcome extends React.Component {
  render() {
    return <h1>Hello, {this.props.name}</h1>;
  }
  
}

<Welcome name="Bob" />; // <h1>Hello, Bob</h1>