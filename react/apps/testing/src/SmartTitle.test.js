import React from "react";
import SmartTitle from './SmartTitle';
import {render , shallow} from 'enzyme';

describe('SmartTitle', () => {
	test('should render without errors', () => {
		render( // triggers the render function of the component
			<SmartTitle greeting="Hey there!"/>
		);
	});

	test('should render correctly with greeting', () => {
		const component = render( // component with children
			<SmartTitle greeting="Hey there!"/>
		);

		expect(component).toMatchSnapshot(); // snapshot file
		expect(component).toMatchInlineSnapshot(`
<h2
  style="color:lime"
>
  Hey there! Default Name
</h2>
`); // bad formatting
	});

	test('should shallow render correctly with greeting', () => {
		const component = shallow( // component with children
			<SmartTitle greeting="Hey there!"/>
		).shallow();

		expect(component).toMatchSnapshot(); // snapshot file
	});
});
