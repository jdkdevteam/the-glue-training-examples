import React from 'react';

const NameContext = React.createContext({
  name: 'Default Name',
  changeName: () => {},
});

export default NameContext;
