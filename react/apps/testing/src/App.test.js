import React from "react";
import App from './App';
import {render, shallow, mount} from 'enzyme';

describe('App', () => {
	test('should render correctly', () => {
		const component = render( // component with children
			<App/>
		);

		expect(component).toMatchSnapshot(); // snapshot file
	});

	test('should mount correctly', () => {
		const component = mount( // component with children
			<App/>
		);

		expect(component).toMatchSnapshot(); // snapshot file
	});

	test('should shallow render correctly', () => {
		const component = shallow( // component with children
			<App/>
		);

		expect(component).toMatchSnapshot(); // snapshot file
	});

	test('should update name to Cornel', () => {
		const component = mount( // component with children
			<App/>
		);

		component.find('.App').simulate('click');
		component.instance().changeName();

		expect(component.state('name')).toBe('Cornel');
	});
});
