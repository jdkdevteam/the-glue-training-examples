import React from "react";
import Article from './Article';
import {mount} from 'enzyme';
import SmartTitle from "./SmartTitle";

describe('Article', () => {
	test('should render', () => {
		const component = mount(
				<Article/>
		);

		expect(component.find(SmartTitle).prop('greeting')).toBe('Hello');
		expect(component.find(SmartTitle).props()).toEqual({greeting: 'Hello'});

		// next part is only for showing API. Not working in test.
		// get returns a node
		expect(component.find(SmartTitle).get(0).prop('greeting')).toBe('Hello');

		// returns enzyme wrapper around node
		expect(component.find(SmartTitle).at(0).prop('greeting')).toBe('Hello');

		component.find(SmartTitle).prop('onRemove')();
	});
});
