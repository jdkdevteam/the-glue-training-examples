import React from "react";
import SmartTitle from './SmartTitle';
import Rename from './Rename';

const PinkSection = () => {
  return (
    <section
      style={{
        width: "200px",
        background: "hotpink",
        padding: "24px"
      }}
    >
      <SmartTitle greeting="Hello" />
      <p>I'm a section with some content.</p>
      <Rename />
    </section>
  );
};

const Article = () => {
  return (
    <article
      style={{
        width: "80%",
        height: "100%",
        background: "gray",
        float: "left"
      }}
    >
      <h2>Article</h2>
      <p>Some article content.</p>
      <PinkSection />
    </article>
  );
};
export default Article;
