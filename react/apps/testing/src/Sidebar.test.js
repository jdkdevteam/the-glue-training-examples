import React from "react";
import Sidebar from './Sidebar';
import {render} from 'enzyme';
import NameContext from "./name-context";

describe('Sidebar', () => {
	test('should render correctly with greeting', () => {
		const component = render(
			<NameContext.Provider value={{name: 'cornel'}}>
				<Sidebar greeting="Hey there!"/>
			</NameContext.Provider>
		);

		expect(component).toMatchSnapshot(); // snapshot file
	});
});
