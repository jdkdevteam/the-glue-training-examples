import React from "react";
import NameContext from "./name-context";

const SmartTitle = (props) => {
	return (
		<NameContext.Consumer>
			{({ name }) => {
				return (
					<h2
						style={{
							color: "lime"
						}}
					>
						{props.greeting} {name}
					</h2>
				);
			}}
		</NameContext.Consumer>
	);
};

SmartTitle.defaultProps = {
	greeting: "Hey"
};

export default SmartTitle;
