import React from "react";
import NameContext from "./name-context";

class Sidebar extends React.Component {
  render() {
    return (
      <aside
        style={{
          width: "20%",
          height: "100%",
          background: "royalblue",
          float: "left"
        }}
      >
        <h2>Sidebar</h2>
        <p>Some sidebar content.</p>
        <div>
          My name is: <pre>{this.context.name}</pre>
        </div>
      </aside>
    );
  }
}
Sidebar.contextType = NameContext;

export default Sidebar;
