import React, { Component } from "react";
import NameContext from "./name-context";
import "./App.css";

import Sidebar from "./Sidebar";
import Article from "./Article";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "Anon",
      changeName: this.changeName
    };
  }

  changeName = (value) => {
    this.setState({ name: value });
  };

  render() {
    return (
      <NameContext.Provider value={this.state}>
        <div className="App">
          <Sidebar />
          <Article />
        </div>
      </NameContext.Provider>
    );
  }
}

export default App;
