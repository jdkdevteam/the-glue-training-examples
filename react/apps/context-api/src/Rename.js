import React from "react";
import NameContext from "./name-context";

const Rename = () => {
  return (
    <NameContext.Consumer>
      {({ name, changeName }) => {
        return (
          <section>
            <p>Current name: {name}</p>
            <input type="text" onChange={e => changeName(e.target.value)} />
          </section>
        );
      }}
    </NameContext.Consumer>
  );
};

export default Rename;
