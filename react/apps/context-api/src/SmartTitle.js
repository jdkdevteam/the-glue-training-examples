import React from "react";
import NameContext from "./name-context";

class SmartTitle extends React.Component {
  render() {
    return (
      <h2
        style={{
          color: "lime"
        }}
      >
        {this.props.greeting} {this.context.name}
      </h2>
    );
  }
}
SmartTitle.contextType = NameContext;
SmartTitle.defaultProps = {
  greeting: "Hey"
};

export default SmartTitle;
