Exercise: 

Create tests for the App, TodoList and TodoListItem components.

- requirements
	- write a unit test for each component that tests the rendered output, methods and event handlers.

- extra:
    - write a general integration test to make sure the whole app works.
	- make a new TodoItemForm component to create a new todo. Use this to update the state and create a test for this component.
	

	
