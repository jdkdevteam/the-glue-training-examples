import React  from "react";

const TodoListItem = ({todo, removeTodo, onComplete}) => (
    <div className="todo-list__item">
	    <h3>{todo.name} - {todo.assignee}</h3>
	    <div>Done: {todo.done ? 'Yes' : 'No'}</div>

        <button onClick={() => removeTodo(todo)}>Remove</button>
	    {!todo.done && (
		    <button onClick={() => onComplete(todo)}>Complete</button>
	    )}
    </div>
);

export default TodoListItem;
