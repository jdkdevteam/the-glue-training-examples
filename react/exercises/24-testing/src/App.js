import React, {Component} from "react";
import "./App.css";
import TodoList from "./TodoList";
import TodoListItem from "./TodoListItem";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      todos: [{
        id: 1,
        name: 'Create tests for this awesome React project!',
        done: false,
        assignee: 'Cornel'
      }]
    };
  }

  addTodo = () => {
    this.setState({
      todos: [
          ...this.state.todos,
        {
          id: this.state.todos.length + 1,
          name: 'Bring beers!',
          done: false,
          assignee: 'Mike'
        }
      ]
    });
  };

  onComplete = (todo) => {
  	const newTodos = [
	    ...this.state.todos
    ];

  	newTodos.find(stateTodo => todo.id === stateTodo.id).done = true;

    this.setState({
      todos: newTodos
    });
  };

  removeTodo = (todo) => {
    this.setState({
      todos: this.state.todos.filter(stateTodo => todo.id !== stateTodo.id)
    });
  };

  render() {
    return (
       <React.Fragment>
         <button onClick={this.addTodo}>Create new ToDo</button>

         <TodoList>
           {this.state.todos.map(todo => (
               <TodoListItem key={todo.id} todo={todo} removeTodo={this.removeTodo} onComplete={this.onComplete}/>
           ))}
         </TodoList>
       </React.Fragment>
    );
  }
}

export default App;
