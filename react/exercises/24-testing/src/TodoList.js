import React  from "react";

const TodoList = (props) => (
    <div className="todo-list">
	    {props.children}
    </div>
);

export default TodoList;
