import axios from "axios";

export const getPlanets = planetId =>
  axios.get(`https://swapi.co/api/planets/${planetId}/?format=json`);
