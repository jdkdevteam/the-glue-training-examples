export const selectAllPlanets = state => state.planets
export const selectLastViewedPlanetIds = state => state.lastViewedPlanetIds
export const selectFavoritePlanetIds = state => state.favoritePlanetIds

export const selectPlanetsByIds = (state, ids) => {
  const allPlanets = selectAllPlanets(state);
  const planets = ids.reduce((acc, id) => {
    return [
      ...acc,
      ...allPlanets.filter(planet  => {
        const planetId = planet.url.split('/').filter(val => val !== '').pop();
        return id === parseInt(planetId, 10);
      })
    ]
  }, [])

  return planets
}