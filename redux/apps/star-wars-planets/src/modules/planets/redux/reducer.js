const initialState = {
  planets: [],
  favoritePlanetIds: [1, 2], // change one of the id's to error-flow in saga
  lastViewedPlanetIds: [], // will be filled in saga
};

const planetReducer = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default planetReducer;
