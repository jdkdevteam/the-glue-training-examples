import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

class PlanetContainer extends Component {
  getPlanets = () => {
    console.log("Get Planets");
  };
  render() {
    console.log("Planet Props", this.props);

    return (
      <Fragment>
        <h1>Planets!</h1>
        <button onClick={this.getPlanets}>Get Planets</button>
        <hr />
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    planets: state.planets
  };
};

const mapDispatchToProps = { getPlanetsStart: () => ({ type: "YO", id: 2 }) };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlanetContainer);
