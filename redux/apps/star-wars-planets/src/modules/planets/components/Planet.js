import React, { Fragment } from 'react';

const Planet = props => {
  const { name, terrain, climate } = props.planet
  return <Fragment>
    <h3>{ name }</h3>
    <div>Terrain: {terrain}</div>
    <div>Climate: {climate}</div>
  </Fragment>
}
export default Planet