export const readLastViewedPlanetIdsFromCookie = cookie => {
  const lastViewedPlanets = document.cookie.split("; ").reduce((prev, current) => {
    const [name, value] = current.split("=");
    prev[name] = value;
    return prev;
  }, {}).lastViewedPlanets;

  return JSON.parse(lastViewedPlanets);
};