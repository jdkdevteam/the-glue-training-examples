import { combineReducers } from "redux";
import planetsReducer from "../modules/planets/redux/reducer";

const reducers = combineReducers({
  planets: planetsReducer
});

const rootReducer = (store, action) => {
  // handle global state here
  return reducers(store, action);
};

export default rootReducer;
