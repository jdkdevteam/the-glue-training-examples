import { createStore, applyMiddleware, compose } from "redux";
import logger from "redux-logger";
// import createSagaMiddleware from "redux-saga";

import reducers from "./reducers";
// import planetSaga from "../modules/planets/redux/saga";

// const sagaMiddleware = createSagaMiddleware();

export const store = createStore(
  reducers,
  compose(
    applyMiddleware(logger /*, sagaMiddleware*/),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);

// sagaMiddleware.run(planetSaga)
