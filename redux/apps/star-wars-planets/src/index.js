import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import { store } from "./redux/config";
import PlanetsContainer from "./modules/planets/PlanetContainer";
import "./index.css";

document.cookie = `lastViewedPlanets=[3, 4];expires=${new Date('2050-01-01').toUTCString()}`

ReactDOM.render(
  <Provider store={store}>
    <PlanetsContainer />
  </Provider>,
  document.getElementById("root")
);
